#!/bin/bash -e
HERE="`dirname $0`"
HERE_FULL="`realpath $HERE`"
docker run --rm -v"$HERE_FULL/../../glm":/root/glm glm-harness:arm64v8 /bin/bash -c "cd /root/glm && \
	cmake -DCMAKE_BUILD_TYPE=Debug -DGLM_TEST_ENABLE=ON -DGLM_TEST_ENABLE_CXX_11=ON . && \
	cmake --build . && \
	ctest"

